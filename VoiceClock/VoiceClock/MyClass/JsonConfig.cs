﻿using System;
using System.Text;
using System.Runtime.Serialization;
using System.IO;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;

namespace VoiceClock.MyClass
{
    public class JsonConfig : INotifyPropertyChanged
    {
        private const string JSON_FILE = "config.json";
        public static JsonConfig Instance { get; } = new JsonConfig();

        #region define json data class
        // Type created for JSON at <<root>> --> JsonData
        [DataContract]
        public partial class JsonData
        {
            [DataMember]
            public String DestinationIPAddress { get; set; }

            [DataMember]
            public ushort DestinationPort { get; set; }

            [DataMember]
            public ushort ReceivePort { get; set; }

            [DataMember]
            public ushort CommandReceivePort { get; set; }

            [DataMember]
            public byte RepeatPlaybackCount { get; set; }

            [DataMember]
            public ushort SendInterval { get; set; }

            [DataMember]
            public GenderType Gender { get; set; }

            [DataMember]
            public TimeFormatType TimeFormat { get; set; }

            [DataMember]
            public string Language { get; set; }
        }
        #endregion

        /// <summary>
        /// construct
        /// </summary>
        private JsonConfig()
        {
            // get config data from json file.
            DeSirializeJsonData();
        }

        /// <summary>
        /// write to json file.
        /// </summary>
        private void WriteJsonData()
        {
            //make a json format.(append indents and line feeds)
            StringBuilder sbJson = new StringBuilder();

            try
            {
                sbJson.Append("{\n");
                sbJson.Append("\t\"DestinationIPAddress\": \"" + _Items.DestinationIPAddress + "\",\n");
                sbJson.Append("\t\"DestinationPort\": " + _Items.DestinationPort.ToString() + ",\n");
                sbJson.Append("\t\"ReceivePort\": " + _Items.ReceivePort.ToString() + ",\n");
                sbJson.Append("\t\"CommandReceivePort\": " + _Items.CommandReceivePort.ToString() + ",\n");
                sbJson.Append("\t\"RepeatPlaybackCount\": " + _Items.RepeatPlaybackCount.ToString() + ",\n");
                sbJson.Append("\t\"SendInterval\": " + _Items.SendInterval.ToString() + ",\n");
                sbJson.Append("\t\"Gender\": " + (byte)_Items.Gender + ",\n");
                sbJson.Append("\t\"TimeFormat\": " + (byte)_Items.TimeFormat + ",\n");
                sbJson.Append("\t\"Language\": \"" + _Items.Language + "\"\n");
                sbJson.Append("}");

                File.WriteAllText(JSON_FILE, sbJson.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {

            }
        }

        #region properties
        private JsonData _Items;
        public JsonData Items
        {
            get
            {
                return _Items;
            }
            set
            {
                _Items = value;
                this.OnPropertyChanged("Items");

                //write to json file.
                WriteJsonData();
            }
        }

        public ushort SendInterval
        {
            get { return _Items.SendInterval; }
            set
            {
                _Items.SendInterval = value;
                OnPropertyChanged("SendInterval");
                //write to json file.
                WriteJsonData();

            }
        }

        public GenderType Gender
        {
            get { return _Items.Gender; }
            set
            {
                _Items.Gender = value;
                OnPropertyChanged("Gender");
                //write to json file.
                WriteJsonData();

            }
        }

        public string Language
        {
            get { return _Items.Language; }
            set
            {
                _Items.Language = value;
                OnPropertyChanged("Language");
                //write to json file.
                WriteJsonData();

            }
        }

        private string _CurrentTime;
        public string CurrentTime
        {
            get { return _CurrentTime; }
            set
            {
                _CurrentTime = value;
                OnPropertyChanged("CurrentTime");
            }
        }

        private Visibility _TimeFormatVisibility;
        public Visibility TimeFormatVisibility
        {
            get { return _TimeFormatVisibility; }
            set
            {
                _TimeFormatVisibility = value;
                OnPropertyChanged("TimeFormatVisibility");
            }
        }

        private string _TimeFormatText;
        public string TimeFormatText
        {
            get { return _TimeFormatText; }
            set
            {
                _TimeFormatText = value;
                OnPropertyChanged("TimeFormatText");
            }
        }

        private Brush _CommunicationStatus;
        public Brush CommunicationStatus
        {
            get { return _CommunicationStatus; }
            set
            {
                _CommunicationStatus = value;
                OnPropertyChanged("CommunicationStatus");
            }
        }

        private Brush _GenderColor;
        public Brush GenderColor
        {
            get { return _GenderColor; }
            set
            {
                _GenderColor = value;
                OnPropertyChanged("GenderColor");
            }
        }

        private string _Minute;
        public string Minute
        {
            get { return _Minute; }
            set
            {
                _Minute = value;
                OnPropertyChanged("Minute");
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        ///  notify when property changed.
        /// </summary>
        /// <param name="propertyName"></param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {

            }
        }

        #region get data from json file.
        /// <summary>
        /// public async Task DeSirializeJsonData()
        /// </summary>
        public void DeSirializeJsonData()
        {
            _Items = new JsonData();
            DeSirializeJsonDataAsync();
        }

        /// <summary>
        /// get data from json file
        /// </summary>
        /// <returns>json data object</returns>
        private void DeSirializeJsonDataAsync()
        {
            String strBuf = "";

            try
            {
                try
                {
                    string path = JSON_FILE;
                    if (!File.Exists(path)) throw new Exception();

                    strBuf = File.ReadAllText(path);
                }
                catch (Exception ex)
                {
                    InitializeConfigValues();
                    MessageBox.Show(string.Format("[{0}] file not found or not correct. initialize config data to default values.\r\n{1}", JSON_FILE, ex.Message));
                    return;
                }

                try
                {
                    _Items = JsonCommon.GetObjectFromJson(strBuf, typeof(JsonData)) as JsonData;
                }
                catch (Exception ex)
                {
                    InitializeConfigValues();
                    Debug.WriteLine(ex.Message);
                    return;
                }


                return;

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return;
            }
            finally
            {

            }
        }

        /// <summary>
        /// initialize config data to default values.
        /// </summary>
        private void InitializeConfigValues()
        {
            _Items.DestinationIPAddress = "";
            _Items.DestinationPort = 46000;
            _Items.ReceivePort = 46210;
            _Items.CommandReceivePort = 46200;
            _Items.RepeatPlaybackCount = 1;
            _Items.SendInterval = 30;
            _Items.Gender = GenderType.Female;
            _Items.TimeFormat = TimeFormatType.Hour_12;
            _Items.Language = "en-US";
        }
        #endregion

    }
}
