﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Globalization;
using Microsoft.VisualBasic;

using HzClientTcpCommunication;

namespace VoiceClock.MyClass
{
    #region define enum
    public enum GenderType : byte
    {
        Female = 0,
        Male
    }

    public enum TimeFormatType : byte
    {
        Hour_12 = 0,
        Hour_24
    }
    #endregion

    public class VoiceClockProc : IDisposable
    {
        private const string LANG_JP = "ja-jp";
        public readonly Brush StatusDefault = new SolidColorBrush(Colors.Gray);
        public readonly Brush StatusActive = new SolidColorBrush(Colors.Lime);
        public readonly Brush GenderMale = new SolidColorBrush(Colors.DodgerBlue);
        public readonly Brush GenderFemale = new SolidColorBrush(Colors.HotPink);
        private JsonConfig jconfig = JsonConfig.Instance;
        private bool flg_stop = false;
        private bool flg_term = false;

        public CancellationTokenSource TokenSource { get; }
        public CancellationToken CancelToken { get;}

        // for sending clock message.
        private ClientTcpCommunication clt_tcp_comm = new ClientTcpCommunication();

        // for receiving voice commands.
        private ClientTcpCommunication cmd_rcv_com = new ClientTcpCommunication();

        public Task ThWorker { get; set; }

        #region dispose
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                disposed = true;
                if (disposing)
                {
                    if (TokenSource != null)
                        TokenSource.Dispose();
                }
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }

        ~VoiceClockProc()
        {
            Dispose(false);
        }
        #endregion

        public VoiceClockProc(string ip, ushort port)
        {
            // for canceling worker task.
            TokenSource = new CancellationTokenSource();
            CancelToken = TokenSource.Token;

            //get cultureinfo(lang code name -> hex).
            CultureInfo CurrentCultureInfo = new CultureInfo(jconfig.Language, false);

            // for sending clock message.
            clt_tcp_comm.ServerIP = ip;
            clt_tcp_comm.ServerPortNo = port;
            clt_tcp_comm.ReceivePort = jconfig.Items.ReceivePort;
            clt_tcp_comm.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity.NoNeed;
            clt_tcp_comm.ReqSendDataSpeechRepeat = jconfig.Items.RepeatPlaybackCount;
            clt_tcp_comm.ReqSendDataSpeechLocaleId = (ushort)CurrentCultureInfo.LCID;
            clt_tcp_comm.startAsynchronousListener();

            // for receiving voice commands.
            cmd_rcv_com.ReceivePort = jconfig.Items.CommandReceivePort;
            cmd_rcv_com.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity.NoNeed;
            cmd_rcv_com.ReqSendDataSpeechLocaleId = (ushort)CurrentCultureInfo.LCID;
            cmd_rcv_com.startAsynchronousListener();

            //Regist event handler when asynchronous message received.(if you need)
            clt_tcp_comm.evNotifyMessageEvent += new delegateNotifyMessageEvent(NotifyCommunicatonMessage);
            clt_tcp_comm.evNotifyReceivedDisConnectEvent += new delegateNotifyReceivedDisConnectEvent(NotifyReceivedDisconnect);
            cmd_rcv_com.evNotifySendSpeechRecognitionCommand += new delegateNotifySendSpeechRecognitionCommand(RcvSendSpeechRecognitionCommand);

            //initialize controls and etc.
            jconfig.CommunicationStatus = StatusDefault;
            jconfig.CurrentTime = "00:00:00";
            jconfig.TimeFormatText = "AM";
            if (jconfig.Items.TimeFormat == TimeFormatType.Hour_24)
                jconfig.TimeFormatVisibility = System.Windows.Visibility.Collapsed;
            else
                jconfig.TimeFormatVisibility = System.Windows.Visibility.Visible;
            // gender of voice synthesizer.
            SetGenderInfo(jconfig.Items.Gender);

        }

        #region Remote Communication Event(callback funtions)
        /// <summary>
        /// When disconected.（received a asynchronous message from access point or when occurred on receiving thread for some reasons.)
        /// </summary>
        /// <param name="msg">message</param>
        /// <param name="st">status</param>
        private void NotifyReceivedDisconnect(string msg, int st)
        {
            Debug.WriteLine(string.Format("{0}, Status[0x{1:x2}]\r\n", msg, st));
        }

        /// <summary>
        /// Error notify when occurred on connecting.
        /// </summary>
        /// <param name="msg">message</param>
        /// <param name="msgId">message id</param>
        /// <param name="errCode">error code</param>
        private void NotifyCommunicatonMessage(string msg, int msgId, int errCode)
        {
            Debug.WriteLine(string.Format("Message ID[{0}], Error Code[0x{1:x2}], {2}\r\n", msgId, errCode, msg));
        }

        /// <summary>
        /// notify when received a SendSpeechRecognitionCommand mmessage.
        /// </summary>
        /// <param name="cmdInfo">SpeechRecognitionCommandInfo class object</param>
        private async void RcvSendSpeechRecognitionCommand(SpeechRecognitionCommandInfo cmd_info)
        {
            Debug.WriteLine(string.Format("Received 'send speech recognition command' message;\r\n ip[{0}], port[{1}], mode[{2}],  command[{3}], time stamp[{4}]",
                cmd_info.IpAddr, cmd_info.Port, cmd_info.Mode, cmd_info.Command, cmd_info.Timestamp) + "\r\n");

            string announce = "";

            // check received command
            if (cmd_info.Command.ToLower() == "start voice clock")
            {
                //start voice clock.
                flg_stop = false;
                announce = Properties.Resources.StartVoiceClock;
                //update status lamp(lime)
                jconfig.CommunicationStatus = StatusActive;
            }
            else if(cmd_info.Command.ToLower() == "stop voice clock")
            {
                //stop voice clock.
                flg_stop = true;
                announce = Properties.Resources.StopVoiceClock;
                //update status lamp(gray)
                jconfig.CommunicationStatus = StatusDefault;
            }
            else if(cmd_info.Command.ToLower() == "terminate voice clock process")
            {
                //shutdown app.
                flg_stop = true;
                flg_term = true;
                announce = Properties.Resources.ShutdownApp;
            }
            else
            {
                //confirm that keywords are included in the command.(free text command)
                MatchCollection vals;
                string val;
                ushort alarm_interval = 30;
                if (Regex.IsMatch(cmd_info.Command.ToLower(), string.Format(@"^(?=.*{0:s})(?=.*[\d]+)(?=.*({1:s}|{2:s}))",
                    Properties.Resources.Interval, Properties.Resources.Change, Properties.Resources.Set)))
                {
                    //sending interval command
                    vals = Regex.Matches(cmd_info.Command, @"[\d]+");
                    Regex num_rg = new Regex("[０-９]+");
                    val = num_rg.Replace(vals[0].Value, Full2Harlf);
                    ushort.TryParse(val, out alarm_interval);
                    if (alarm_interval >= 1 && alarm_interval <= 60)
                    {
                        announce = string.Format(Properties.Resources.SetSendingInterval, alarm_interval);
                        //update sending interval
                        jconfig.SendInterval = alarm_interval;
                    }
                    else
                        announce = string.Format(Properties.Resources.WarningSendingInterval, alarm_interval);
                }
                else if (Regex.IsMatch(cmd_info.Command.ToLower(), string.Format(@"^(?=.*({0:s}|{1:s}))(?=.*({2:s}|{3:s}))",
                    Properties.Resources.Male, Properties.Resources.Female, Properties.Resources.Change, Properties.Resources.Set)))
                {
                    //gender command
                    vals = Regex.Matches(cmd_info.Command, string.Format(@"({0:s}|{1:s})", Properties.Resources.Male, Properties.Resources.Female));
                    GenderType type = GenderType.Female;
                    if (vals[0].Value == Properties.Resources.Male)
                        type = GenderType.Male;

                    announce = string.Format(Properties.Resources.SetGender, vals[0].Value);
                    //update gender
                    SetGenderInfo(type);
                }
                else
                {
                    //command not found.
                    announce = Properties.Resources.CommandNotFound;
                }
            }

            Debug.WriteLine(announce);

            try
            {
                // send speech data.
                await SendResponce(cmd_info, announce);
                if (flg_term)
                    TokenSource.Cancel();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// convert from full charactors to half charactors.
        /// </summary>
        /// <param name="m">original charactors object</param>
        /// <returns></returns>
        private string Full2Harlf(Match m)
        {
            return Strings.StrConv(m.Value, VbStrConv.Narrow, 0);
        }

        /// <summary>
        /// set gender of voice synthesizer.
        /// <param name="type">gender type</param>
        /// </summary>
        private void SetGenderInfo(GenderType type)
        {
            if (type == GenderType.Female)
            {
                clt_tcp_comm.ReqSendDataSpeechGender = HzSpeechGender.Female;
                cmd_rcv_com.ReqSendDataSpeechGender = HzSpeechGender.Female;
                jconfig.Gender = GenderType.Female;
                jconfig.GenderColor = GenderFemale;
            }
            else
            {
                clt_tcp_comm.ReqSendDataSpeechGender = HzSpeechGender.Male;
                cmd_rcv_com.ReqSendDataSpeechGender = HzSpeechGender.Male;
                jconfig.Gender = GenderType.Male;
                jconfig.GenderColor = GenderMale;
            }
        }


        /// <summary>
        /// send a message of the current time.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private void SendTimeMessage(string msg)
        {
            Task.Run(async () =>
            {
                clt_tcp_comm.ReqSendDataText = msg;
                string time_stamp = await clt_tcp_comm.sendSpeechDataEx();
                if (time_stamp != null)
                {
                    Debug.WriteLine("success. timestamp[{0:s}]", time_stamp);
                }
                else
                {
                    Debug.WriteLine("failed to send.");
                }
            });
        }

        /// <summary>
        /// send a responce message when recieved 'Send Speech Recognition Command'.
        /// </summary>
        /// <param name="cmd_info"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private async Task<string> SendResponce(SpeechRecognitionCommandInfo cmd_info, string msg)
        {
            cmd_rcv_com.ServerPortNo = cmd_info.Port;
            cmd_rcv_com.ServerIP = cmd_info.IpAddr;
            cmd_rcv_com.ReqSendDataText = msg;
            return await cmd_rcv_com.sendSpeechDataEx();
        }

        /// <summary>
        /// start a worker task for sending current time.
        /// </summary>
        public void StartWorkerClock()
        {
            ThWorker = Task.Run(async () =>
            {
                Debug.WriteLine("start worker thread.");
                StringBuilder msg = new StringBuilder();
                flg_stop = true;
                while (true)
                {
                    //already canceled?
                    CancelToken.ThrowIfCancellationRequested();

                    try
                    {
                        DateTime nowtime = DateTime.Now;

                        if (!flg_stop && nowtime.Second == 0 && (nowtime.Minute % jconfig.SendInterval == 0))
                        {
                            Debug.WriteLine("alarm time.[{0}]", nowtime.ToString());
                            msg.Clear();
                            if (jconfig.Items.TimeFormat == TimeFormatType.Hour_24)
                            {
                                msg.Append(string.Format(Properties.Resources.CurrentTimeMessage, nowtime.ToString("HH:mm")));
                            }
                            else
                            {
                                if(jconfig.Language.ToLower() == LANG_JP)
                                {
                                    if (nowtime.Hour == 0 || nowtime.Hour == 12)
                                        msg.Append(string.Format(Properties.Resources.CurrentTimeMessage, nowtime.ToString("tt、00:mm")));
                                    else
                                        msg.Append(string.Format(Properties.Resources.CurrentTimeMessage, nowtime.ToString("tt、hh:mm")));
                                }
                                else
                                {
                                        msg.Append(string.Format(Properties.Resources.CurrentTimeMessage, nowtime.ToString("hh:mm") +
                                            nowtime.ToString(" tt", CultureInfo.CreateSpecificCulture(jconfig.Language))));
                                }
                            }

                            //send the current time message.
                            SendTimeMessage(msg.ToString());
                        }

                        Debug.WriteLine("{0:D2}:{1:D2}:{2:D2}", nowtime.Hour, nowtime.Minute, nowtime.Second);
                        if(jconfig.Items.TimeFormat == TimeFormatType.Hour_24)
                        {
                            jconfig.CurrentTime = nowtime.ToString("HH:mm:ss");
                        }
                        else
                        {
                            if ((nowtime.Hour == 0 || nowtime.Hour == 12) && jconfig.Language.ToLower() == LANG_JP)
                                jconfig.CurrentTime = nowtime.ToString("00:mm:ss");
                            else
                                jconfig.CurrentTime = nowtime.ToString("hh:mm:ss");
                            jconfig.TimeFormatText= nowtime.ToString("tt", CultureInfo.CreateSpecificCulture(jconfig.Language));
                        }

                        await Task.Delay(1000);

                        if (CancelToken.IsCancellationRequested)
                            CancelToken.ThrowIfCancellationRequested();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                        break;
                    }
                }
            }, TokenSource.Token);

        }


    }
}
