﻿using System;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;

namespace VoiceClock.MyClass
{
    public static class JsonCommon
    {
        /// <summary>
        /// deserialize json data file.
        /// </summary>
        /// <param name="jsonString">stream data string</param>
        /// <param name="t">json data type</param>
        /// <returns>deserialized data</returns>
        public static object GetObjectFromJson(string jsonString, Type t)
        {
            var serializer = new DataContractJsonSerializer(t);
            var jsonBytes = Encoding.Unicode.GetBytes(jsonString);
            var sr = new MemoryStream(jsonBytes);
            return serializer.ReadObject(sr);
        }

    }
}
