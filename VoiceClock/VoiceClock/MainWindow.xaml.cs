﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Diagnostics;
using VoiceClock.MyClass;
using System.Globalization;

namespace VoiceClock
{

    /// <summary>
    /// interaction logic of the MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        public JsonConfig jconfig = JsonConfig.Instance;
        private VoiceClockProc vcp;

        public MainWindow()
        {
            //initialize culuture information.
            Properties.Resources.Culture = CultureInfo.GetCultureInfo(jconfig.Language);

            InitializeComponent();

            // make the window moveable. 
            this.MouseLeftButtonDown += (sender, e) => { this.DragMove(); };

        }

        /// <summary>
        /// window loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //set binding.
            MainGrid.DataContext = jconfig;

            //create instance. (Haruzira's ip and port)
            vcp = new VoiceClockProc(jconfig.Items.DestinationIPAddress, jconfig.Items.DestinationPort);


            //start a worker task for current time processing.
            vcp.StartWorkerClock();


            //start a task for waiting for worker task.
            StartWorkerWaitingTask();

            Debug.WriteLine("end loaded event.");

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            vcp.TokenSource.Cancel();
            vcp.Dispose();
        }

        /// <summary>
        /// start a working task.
        /// </summary>
        private void StartWorkerWaitingTask()
        {
            //waiting for working task until it's completed or canceled.
            Task.Run(() =>
            {
                try
                {
                    vcp.ThWorker.Wait();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("canceld worker task.\r\n" + ex.Message);

                }
                finally
                {
                    if (vcp.CancelToken.IsCancellationRequested)
                        Debug.WriteLine("canceld worker task.");
                    vcp.TokenSource.Dispose();
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        Application.Current.Shutdown();
                    }));
                    Debug.WriteLine("application exit.");
                }
            });

        }

        /// <summary>
        /// make the window minimize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// exit application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

    }
}
