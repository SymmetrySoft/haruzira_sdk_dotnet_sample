===============================================================================================
= Haruzira SDK for .NET Sample Program.   Copyright (c) 2016 Symmetry Soft
===============================================================================================
1. Overview
    For performing network communication with UWP APP "Haruzira", is a sample program that describes how to use the SDK for .NET.

2. System Requirements
    Your system needs to has installed .Net Framework 4.5 and more.
    Environment that confirmed the operation on our side.(OS version)
    a)Windows 7
    b)Windows 10

3. Preparation
    to start up "Haruzira UWP APP" on the device for access point. and run the server.
    *UWP APP can not be basically that loopback(127.0.0.1). you need to work start-up on the other device or a virtual PC.

	If you want to receive voice command from Haruzira, need to set the Haruzira's following menu in advance.
      - 'Remote Devices' 
	        Device name: "Weather Information"
	        Encryption key: "test23key" for sample app. If you no need encryption, not enter any.
	        Encryption algorithm: "AES-CbcPkcs7"
	        IP address: Ex. "192.168.1.5"
	        Port number: default port "46100"

      - 'Remote Voice Commands' 
	        Command name: "Weather Information"
		    Recognition phrases: such as "weather;tell me status;temperature" etc. (free words)
		    Description: optional
    


4. How to start up
    Double click a "HzClientSample.exe"
    *It will be localized in Japanese or in English when start up.
     If you want to localize in English, rename "ja-JP" folder or delete.
     If you are asked for firewall permission, allow it.


5. How to operation
    After start up, You need to modify of some informations for the communication.
    an access point needs Ip address and port number.
    and a connection source needs a listener port number to receive asynchronous messages.

    Examples)
    ex.1) Enter in the text area. and click "send data", it will be made a speech by Haruzira.
    
    ex.2) When button of "Send RSS DATA" was sclicked , it will be made a speech at 15 minutes interval by Haruzira.
       *You can select for interval time.


6. About SDK
    It is possible to control the speech synthesis engine, if be modified various speech options.
    
    Examples)
    ex.1)It is possible to change the speech synthesis engine, if modify language code or gender option. 
    ex.2)It is possible to repeat playing, if modify repeat count option.
    ex.3)It is possible to make a speech by interrupt when is playing in priority of "Normal", if modify priority option to "High".
    ex.4)It is possible to make a secure communication by using account authentication or encryption data, if modify account information or encryption key option.
    *if you need more information about SDK, make reference to a SDK manual or analyze sample program.
	  https://haruzirasdke.wpblog.jp/


