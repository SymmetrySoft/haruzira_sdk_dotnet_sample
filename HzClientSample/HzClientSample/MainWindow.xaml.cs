﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Xml;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Syndication;
//using System.ServiceModel.Web;
using HzClientTcpCommunication;

namespace HzClientSample
{
    /// <summary>
    /// MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region variable&constatnt
        private const ushort DEFAULT_RECEIVE_PORT = 46100;    //Listener  default port for receiving asynchronous message.
        private const ushort DEFAULT_SERVER_PORT = 46000;     //Server default port
        private ClientTcpCommunication _cltTcpComm = null;
        private CancellationTokenSource _tokenSource = null;
        private CancellationToken _token;
        private XmlReader _rdr = null;
        private SyndicationFeed _feed = null;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _cltTcpComm = new ClientTcpCommunication();
            //Regist event handler when asynchronous message received.(if you need)
            _cltTcpComm.evNotifyMessageEvent += new delegateNotifyMessageEvent(notifyCommunicatonMessage);
            _cltTcpComm.evNotifyReceivedDisConnectEvent += new delegateNotifyReceivedDisConnectEvent(notifyReceivedDisconnect);
            _cltTcpComm.evNotifyCompeteSpeech += new delegateNotifyCompleteSpeech(rcvNotifyCompleteSpeech);
            _cltTcpComm.evNotifySendSpeechRecognitionCommand += new delegateNotifySendSpeechRecognitionCommand(rcvSendSpeechRecognitionCommand);

            //If you want to receive voice command, need to Start listener.
            //Also if you want to decrypt that be encrypted voice command, need to set 'ReceivedDataDecryptionKey' property.
            ushort rcvPort = DEFAULT_RECEIVE_PORT;
            if (!ushort.TryParse(txtLisnerPort.Text, out rcvPort))
                rcvPort = DEFAULT_RECEIVE_PORT;
            _cltTcpComm.ReceivePort = rcvPort;
            _cltTcpComm.startAsynchronousListener();
            _cltTcpComm.ReceivedDataDecryptionKey = txtDecryptionKey.Text.Trim();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //cancel listener thread.
            _cltTcpComm.cancelAsynchronousListener();
        }

        #region Remote Communication Event(callback funtions)
        /// <summary>
        /// When disconected.（received a asynchronous message from access point or when occurred on receiving thread for some reasons.)
        /// </summary>
        /// <param name="msg">message</param>
        /// <param name="st">status</param>
        private void notifyReceivedDisconnect(string msg, int st)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                txtNotify.Text += string.Format("{0}, Status[0x{1:x2}]\r\n", msg, st);
            }), DispatcherPriority.Send);
        }

        /// <summary>
        /// Error notify when occurred on connecting.
        /// </summary>
        /// <param name="msg">message</param>
        /// <param name="msgId">message id</param>
        /// <param name="errCode">error code</param>
        private void notifyCommunicatonMessage(string msg, int msgId, int errCode)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                txtNotify.Text += string.Format("Message ID[{0}], Error Code[0x{1:x2}], {2}\r\n", msgId, errCode, msg);
            }), DispatcherPriority.Send);
        }

        /// <summary>
        /// notify when received a NotifyCompleteSpeech message.(asynchronous message)
        /// </summary>
        /// <param name="result"></param>
        /// <param name="time_stamp"></param>
        private void rcvNotifyCompleteSpeech(int result, string time_stamp)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                txtNotify.Text += string.Format(Properties.Resources.ReceivedNotifyCompleteSpeechMsg, result, time_stamp) + "\r\n";
            }), DispatcherPriority.Send);
        }

        /// <summary>
        /// notify when received a SendSpeechRecognitionCommand mmessage.
        /// </summary>
        /// <param name="cmdInfo">SpeechRecognitionCommandInfo class object</param>
        private void rcvSendSpeechRecognitionCommand(SpeechRecognitionCommandInfo cmdInfo)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                txtNotify.Text += string.Format(Properties.Resources.ReceivedNotifySendSpeechRecognitionCommandMsg, 
                    cmdInfo.IpAddr, cmdInfo.Port, cmdInfo.Mode, cmdInfo.Command, cmdInfo.Timestamp) + "\r\n";

                //send speech data.
                string msg = "";
                if (cmdInfo.Command.ToLower() == Properties.Resources.CommandWeather)
                    msg = Properties.Resources.WeatherInformationMsg;
                else
                    msg = string.Format(Properties.Resources.WarningInvalidCommandDataReceviedMsg, cmdInfo.Command);
                _cltTcpComm.ServerPortNo = cmdInfo.Port;
                _cltTcpComm.ServerIP = cmdInfo.IpAddr;
                SendSpeechData(Properties.Resources.MyDeviceName + "\r\n" + msg);
            }), DispatcherPriority.Send);
        }
        #endregion


        private void chkAccount_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)chkAccount.IsChecked)
            {
                txtAccountName.IsEnabled = true;
                txtPassword.IsEnabled = true;
                txtEncryptionKey.IsEnabled = true;
            }else
            {
                txtAccountName.IsEnabled = false;
                txtPassword.IsEnabled = false;
                txtEncryptionKey.IsEnabled = false;
            }
        }

        /// <summary>
        /// Set access point ip and port.
        /// </summary>
        private void SetServerInfo()
        {
            int port = 0;
            string serverIp = "";

            //Set access point ip and port.(you must) 
            if (!int.TryParse(txtServerPort.Text.Trim(), out port))
                port = DEFAULT_SERVER_PORT;
            if (txtServerName.Text.Trim() != "")
                serverIp = txtServerName.Text.Trim();
            else
                throw new Exception(string.Format("Please enter ip or host name for access point.[{0}]", serverIp));
            _cltTcpComm.ServerPortNo = port;
            _cltTcpComm.ServerIP = serverIp;
        }

        /// <summary>
        /// Set to send speech data 
        /// </summary>
        /// <param name="speechTxt"></param>
        private async void SendSpeechData(string speechTxt)
        {
            ushort rcvPort = DEFAULT_RECEIVE_PORT;

            try
            {
                //txtConnectStatus.Text = "ON LINE";

                //Set listener port number for asynchronous message.(if you need)
                if (!ushort.TryParse(txtLisnerPort.Text, out rcvPort))
                    rcvPort = DEFAULT_RECEIVE_PORT;
                _cltTcpComm.ReceivePort = rcvPort;
                //Set time out value for receiving Ack message.(if you need)
                _cltTcpComm.ReceiveAckTimeOut = 40000;  //(msec) this example value is 40 seconds.(default value 30 seconds)
                //Set account information.(if you need)
                if ((bool)chkAccount.IsChecked)
                {
                    _cltTcpComm.ReqSendDataAccountName = txtAccountName.Text.Trim();
                    _cltTcpComm.ReqSendDataPasswd = txtPassword.Text.Trim();
                    _cltTcpComm.ReqSendDataEncryptKey = txtEncryptionKey.Text.Trim();
                }
                else
                {
                    _cltTcpComm.ReqSendDataAccountName = null;
                    _cltTcpComm.ReqSendDataPasswd = null;
                    _cltTcpComm.ReqSendDataEncryptKey = null;
                }
                //Set speech languge code.(if you need)
                string[] strLangCode = cmbLangCode.Text.Split(':');
                ushort localeId = 0;
                ushort.TryParse(strLangCode[0], out localeId);
                _cltTcpComm.ReqSendDataSpeechLocaleId = localeId;
                //Set speech text mode.(you must)
                _cltTcpComm.ReqSendDataSpeechMode = (HzSpeechTextMode)cmbTextMode.SelectedIndex;
                //Set speech priority.(if you need)
                _cltTcpComm.ReqSendDataSpeechLevel = (HzSpeechLevel)cmbPriority.SelectedIndex;
                //Set speech gender.(if you need)
                _cltTcpComm.ReqSendDataSpeechGender = (HzSpeechGender)cmbGender.SelectedIndex;
                //Set speech repeat count.(if you need)
                int repeat = 0;
                int.TryParse(txtRepeat.Text.Trim(), out repeat);
                _cltTcpComm.ReqSendDataSpeechRepeat = (byte)repeat;
                //Set SpeechCompletionNotice necessity.(if you need)
                if((bool)radCompletionNeed.IsChecked)
                    _cltTcpComm.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity.Need;
                else
                    _cltTcpComm.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity.NoNeed;
                //Set speech text data.(you must)
                _cltTcpComm.ReqSendDataText = speechTxt;

                //Send speech data.(return time stamp when send sccess)
                string timeStamp = await _cltTcpComm.sendSpeechDataEx();

                if (timeStamp == null)
                {
                    throw new Exception(string.Format("Error Code：[0x{0:x2}]", _cltTcpComm.ReceiveStatus));
                }

                Debug.WriteLine("Sent Timestamp[{0}]", (object)timeStamp);
                txtNotify.Text += string.Format("Sent Timestamp[{0}]\r\n", (object)timeStamp);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                txtNotify.Text += ex.Message + "\r\n";
            }
            finally
            {
                if (!btnSendData.IsEnabled)
                    btnSendData.IsEnabled = true;
            }
        }

        /// <summary>
        /// Send Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSendData_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnSendData.IsEnabled = false;

                //Set access point ip and port.(you must) 
                SetServerInfo();

                //Set to send speech data.
                SendSpeechData(txtSpeech.Text.Trim());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {

            }

        }

        /// <summary>
        /// Send RSS Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnSendRssData_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                _tokenSource = new CancellationTokenSource();
                _token = _tokenSource.Token;

                //Set access point ip and port.(you must) 
                SetServerInfo();

                await Task.Run(async() => {

                    //get interval time and rss feed url.
                    int interval = 0;
                    string[] rssArray = null;
                    Dispatcher.Invoke(new Action(() =>
                    {
                        int.TryParse(cmbRssInterval.Text, out interval);
                        rssArray = cmbRssFeed.Text.Split('|');
                    }));

                    interval *= 60000;
                    string url = rssArray[0];
                    if (rssArray.Length > 1) url = rssArray[1];
                    string rss = "";

                    //loop until stop by selected interval time.
                    while (true)
                    {

                        _rdr = XmlReader.Create(url);
                        _feed = SyndicationFeed.Load(_rdr);
                        rss = _feed.Title.Text + "\r\n";
                        //rss += _feed.Description.Text + "\r\n";

                        foreach (SyndicationItem item in _feed.Items)
                        {
                            //Join item title.(replace(" " -> "、") for break time)
                            if(rssArray[0] == "Japanese")
                                rss += (item.Title.Text + "\r\n").Replace(" ", "、");
                            else
                                rss += item.Title.Text + "\r\n";
                            rss += (item.Summary != null ? item.Summary.Text : "") + "\r\n";
                            //rss += "link:" + (item.Links.Count > 0 ? item.Links[0].Uri.AbsolutePath : "") + "\r\n\r\n";
                            //rss += "link:" + (item.Links.Count > 0 ? item.Links[0].Uri.AbsoluteUri : "") + "\r\n\r\n";
                        }

                        Dispatcher.Invoke(new Action(() =>
                        {
                            txtSpeech.Text = rss;
                            btnSendRssData.IsEnabled = false;
                            //Set to send speech data.
                            SendSpeechData(rss);
                        }));

                        _feed = null;

                        //if interval time is zero, it doesn't repeat.
                        if (interval == 0) break;

                        await Task.Delay(interval, _token);     //defult: 0 (no repeat)
                        if (_token.IsCancellationRequested)
                                break;
                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Dispatcher.Invoke(new Action(() =>
                {
                    btnStopRssData_Click(sender, e);
                }));
            }

        }

        /// <summary>
        /// Stop RSS Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStopRssData_Click(object sender, RoutedEventArgs e)
        {
            if(_tokenSource != null)
            {
                _tokenSource.Cancel();
                _rdr.Close();
                _rdr.Dispose();
            }
            if (!btnSendRssData.IsEnabled)
                btnSendRssData.IsEnabled = true;
        }

    }
}
