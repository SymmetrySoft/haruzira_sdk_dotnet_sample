﻿using System;
using System.Windows;
using System.Windows.Threading;
using HzClientTcpCommunication;

namespace SimpleCommandReceiver
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        //create instance
        ClientTcpCommunication cltTcpComm = new ClientTcpCommunication();

        public MainWindow()
        {
            InitializeComponent();

            //regist callback function.
            cltTcpComm.evNotifySendSpeechRecognitionCommand += new delegateNotifySendSpeechRecognitionCommand(rcvSendSpeechRecognitionCommand);

            //Set listener port number and start listener.
            cltTcpComm.ReceivePort = 46100;
            cltTcpComm.startAsynchronousListener();
        }


        // Callback funtion
        private void rcvSendSpeechRecognitionCommand(SpeechRecognitionCommandInfo cmdInfo)
        {
            Dispatcher.BeginInvoke(new Action(async () =>
            {
                //txtNotify is a TextBox control.
                TxtNotify.Text += string.Format("ip[{0}], port[{1}], command[{2}], time stamp[{3}]",
                    cmdInfo.IpAddr, cmdInfo.Port, cmdInfo.Command, cmdInfo.Timestamp) + "\r\n";

                //check received 
                string msg;
                if (cmdInfo.Command.ToLower() == "weather information")
                    msg = "It's sunny today.\r\nBut, It will probably be rainy tomorrow.";
                else
                    msg = string.Format("Invalid command name. [{0}]\r\nI can't work your command.", cmdInfo.Command);

                //send speech data.
                cltTcpComm.ServerPortNo = cmdInfo.Port;
                cltTcpComm.ServerIP = cmdInfo.IpAddr;
                cltTcpComm.ReqSendDataCompletionNoticeNecessity = HzSpCompletionNoticeNecessity.NoNeed;
                cltTcpComm.ReqSendDataText = msg;
                await cltTcpComm.sendSpeechDataEx();
            }), DispatcherPriority.Send);
        }

    }
}
